package com.otaghak.tvmazetest.models;

import com.otaghak.tvmazetest.Utils;

public class Country implements Model {
    public String name;
    public String code;

    @Override
    public String toString() {
        if (!Utils.checkNull(code)) {
            return "(" + code + ")";
        } else {
            return "(" + name + ")";
        }
    }
}
