package com.otaghak.tvmazetest.models;

public class ShowRating implements Model {
    public Double average;

    public float getAverageRating() {
        if (average == null) {
            return 0;
        } else {
            return (float) (average / 2);

        }
    }

    @Override
    public String toString() {
        if (average == null) {
            return "NA";
        } else {
            return String.valueOf(average) + "/10";
        }
    }
}
