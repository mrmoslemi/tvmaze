package com.otaghak.tvmazetest.models;

import com.otaghak.tvmazetest.Utils;

import java.util.List;

public class Schedule implements Model {
    public String time;
    public List<String> days;

    @Override
    public String toString() {
        StringBuilder days = new StringBuilder();
        if (this.days != null) {
            for (String day : this.days) {
                days.append(day).append("s, ");
            }
        }
        String toReturn = days.toString();
        if (toReturn.endsWith(", ")) {
            toReturn = toReturn.substring(0, toReturn.length() - 2);
        }
        if (!Utils.checkNull(time)) {
            toReturn += " at " + time;
        }
        return toReturn;
    }
}
