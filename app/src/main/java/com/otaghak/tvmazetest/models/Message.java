package com.otaghak.tvmazetest.models;

import com.otaghak.tvmazetest.R;

public class Message implements Model {
    private static Message queryIsEmpty;
    private static Message resultIsEmpty;
    public int imageResId;
    public int titleResId;
    public int subTitleResId;

    public static Message getQueryIsEmptyMessage() {
        if (queryIsEmpty == null) {
            queryIsEmpty = new Message();
            queryIsEmpty.imageResId = R.drawable.ic_browsing_accent;
            queryIsEmpty.titleResId = R.string.start_browsing;
            queryIsEmpty.subTitleResId = R.string.try_searching_something;
        }
        return queryIsEmpty;
    }

    public static Message getResultIsEmptyMessage() {
        if (resultIsEmpty == null) {
            resultIsEmpty = new Message();
            resultIsEmpty.imageResId = R.drawable.ic_empty_accent;
            resultIsEmpty.titleResId = R.string.warning;
            resultIsEmpty.subTitleResId = R.string.search_result_empty;
        }
        return resultIsEmpty;
    }
}
