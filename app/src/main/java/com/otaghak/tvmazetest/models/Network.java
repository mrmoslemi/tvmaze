package com.otaghak.tvmazetest.models;

public class Network implements Model {
    public int id;
    public String name;
    public Country country;

    @Override
    public String toString() {
        if (country != null) {
            return name + country.toString();
        } else {
            return name;
        }
    }
}
