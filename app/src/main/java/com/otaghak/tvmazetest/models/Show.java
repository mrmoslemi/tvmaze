package com.otaghak.tvmazetest.models;

import java.util.List;

public class Show implements Model {
    public int id;
    public String name;
    public String type;
    public String language;
    public List<String> genres;
    public String status;
    public String premiered;
    public String officialSite;
    public Schedule schedule;
    public ShowRating rating;
    public Network network;
    public DualImage image;
    public String summary;
}
