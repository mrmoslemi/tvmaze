package com.otaghak.tvmazetest;

import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.FrameLayout;

import com.otaghak.tvmazetest.views.AbstractFragment;
import com.otaghak.tvmazetest.views.searchFragment.SearchFragment;

import java.util.ArrayList;

public class MainView {
    private MainActivity mainActivity;
    private FrameLayout fragmentContainer;
    private ArrayList<AbstractFragment> fragments;
    private boolean paused;

    private static final int ANIMATION_TYPE_NONE = 0;
    private static final int ANIMATION_TYPE_PUSH = 1;
    private static final int ANIMATION_TYPE_POP = 2;


    private View findViewById(int id) {
        return mainActivity.findViewById(id);
    }

    private MainActivity getMainActivity() {
        return mainActivity;
    }

    MainView(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
        fragmentContainer = (FrameLayout) findViewById(R.id.mainView_fragmentContainer);
        pushFragment(new SearchFragment(), ANIMATION_TYPE_NONE);
    }


    public void popFragment() {
        if (!paused) {
            if (fragments.size() <= 1) {
                getMainActivity().handleOsBack();
            } else {
                AbstractFragment deleted = fragments.remove(fragments.size() - 1);
                deleted.kill();
                System.gc();
                AbstractFragment fragment = fragments.get(fragments.size() - 1);

                fragmentTrans(fragment, ANIMATION_TYPE_POP);
            }
        }
    }

    public void pushFragment(AbstractFragment fragment) {
        this.pushFragment(fragment, ANIMATION_TYPE_PUSH);
    }

    private void pushFragment(AbstractFragment fragment, int animationType) {
        if (!paused) {
            if (fragments == null) {
                fragments = new ArrayList<>();
            }
            fragments.add(fragment);
            fragmentTrans(fragment, animationType);
        }
    }

    private void fragmentTrans(AbstractFragment toSwitchTo, int direction) {
        if (!paused) {
            FragmentManager fragmentManager = getMainActivity().getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            switch (direction) {
                case ANIMATION_TYPE_POP:
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                    break;
                case ANIMATION_TYPE_PUSH:
                    fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                    break;
            }
            if (toSwitchTo.getView() != null) {
                fragmentContainer.removeView(toSwitchTo.getView());
            }
            fragmentTransaction.replace(fragmentContainer.getId(), toSwitchTo);
            pause();
            fragmentTransaction.commitNow();
        }
    }

    public boolean hasBack() {
        return fragments.size() > 1;
    }

    public void pause() {
        paused = true;
    }

    public void resume() {
        paused = false;
    }
}
