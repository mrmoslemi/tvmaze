package com.otaghak.tvmazetest;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;

import com.otaghak.tvmazetest.api.API;
import com.otaghak.tvmazetest.api.Request;

public class MainActivity extends AppCompatActivity {
    private API api;
    private MainView mainView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        api = new API(this);
        mainView = new MainView(this);
    }

    public void request(Request request) {
        api.request(request);
    }

    public void handleOsBack() {
        super.onBackPressed();
    }

    @Override
    public void onBackPressed() {
        mainView.popFragment();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mainView.popFragment();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public MainView getMainView() {
        return mainView;
    }

    @Override
    public void onPause() {
        super.onPause();
        mainView.pause();
    }

    @Override
    public void onResume() {
        super.onResume();
        mainView.resume();
    }
}
