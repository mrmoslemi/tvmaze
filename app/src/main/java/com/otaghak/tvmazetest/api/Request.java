package com.otaghak.tvmazetest.api;

import com.google.gson.Gson;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Response;
import com.otaghak.tvmazetest.api.listeners.OnModelListReceivedListener;
import com.otaghak.tvmazetest.api.listeners.OnModelReceivedListener;
import com.otaghak.tvmazetest.api.listeners.OnReceivedListener;
import com.otaghak.tvmazetest.api.utils.StatusCode;
import com.otaghak.tvmazetest.api.utils.URLs;
import com.otaghak.tvmazetest.models.Model;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Request<E extends Model> implements FutureCallback<Response<byte[]>> {

    public static final int METHOD_GET = 0;
    public static final int METHOD_POST = 1;

    private static final int LISTENER_TYPE_MODEL = 2;
    private static final int LISTENER_TYPE_MODEL_LIST = 3;


    private int id;
    private int method;
    private String relativeUrl;
    private String baseUrl;
    private RequestParams params;

    private Class<E> objectsClass;
    private OnReceivedListener listener;
    private int listenerType;

    private static int nextID = 1;


    private Request(int method, String relativeUrl, Class<E> objectsClass, OnReceivedListener listener, int listenerType) {
        this.method = method;
        this.relativeUrl = relativeUrl;
        this.objectsClass = objectsClass;
        this.listener = listener;
        this.listenerType = listenerType;
        this.id = nextID++;
        this.baseUrl = URLs.API_URL;

    }

    public Request(int method, String relativeUrl, Class<E> objectsClass, OnModelReceivedListener<E> listener) {
        this(method, relativeUrl, objectsClass, listener, LISTENER_TYPE_MODEL);
    }

    public Request(int method, String relativeUrl, Class<E> objectsClass, OnModelListReceivedListener<E> listener) {
        this(method, relativeUrl, objectsClass, listener, LISTENER_TYPE_MODEL_LIST);
    }

    public void setParams(RequestParams params) {
        this.params = params;
    }

    public String getAbsoluteUrl() {
        return this.baseUrl.concat(this.relativeUrl);
    }

    public int getMethod() {
        return this.method;
    }

    public int getId() {
        return this.id;
    }

    public OnReceivedListener getListener() {
        return listener;
    }

    public RequestParams getParams() {
        return params;
    }

    public HashMap<String, List<String>> getQuery() {
        if (params == null) {
            return new HashMap<>();
        } else {
            return params.getQuery();
        }
    }

    @Override
    public void onCompleted(Exception e, Response<byte[]> result) {
        handleResponse(e, result);
    }

    private void handleResponse(Exception e, Response<byte[]> result) {
        if (listener.isAlive()) {
            if (e != null) {
                listener.onFailure(id, StatusCode.NO_INTERNET_CONNECTION);
            } else {
                int statusCode = result.getHeaders().code();
                if (StatusCode.isSuccess(statusCode)) {
                    String receivedString = new String(result.getResult());
                    Gson gson = new Gson();
                    ArrayList<E> receivedModelList = new ArrayList<>();
                    switch (listenerType) {
                        case Request.LISTENER_TYPE_MODEL:
                            E receivedModel = gson.fromJson(receivedString, objectsClass);
                            ((OnModelReceivedListener<E>) listener).onSuccess(id, statusCode, receivedModel);
                            break;
                        case Request.LISTENER_TYPE_MODEL_LIST:
                            try {
                                JSONArray receivedModelListJson = new JSONArray(receivedString);
                                for (int i = 0; i < receivedModelListJson.length(); i++) {
                                    String receivedModelString = receivedModelListJson.getJSONObject(i).toString();
                                    receivedModelList.add(gson.fromJson(receivedModelString, objectsClass));
                                }
                            } catch (JSONException ignored) {
                            }
                            ((OnModelListReceivedListener<E>) listener).onSuccess(id, statusCode, receivedModelList);
                            break;
                    }

                } else {
                    listener.onFailure(id, statusCode);
                }
            }
        }


    }
}
