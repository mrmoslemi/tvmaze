package com.otaghak.tvmazetest.api.utils;

public class StatusCode {
    public static final int NO_INTERNET_CONNECTION = -1;

    public static boolean isSuccess(int statusCode) {
        return (statusCode / 100) == 2;
    }
}
