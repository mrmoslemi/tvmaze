package com.otaghak.tvmazetest.api.listeners;

import com.otaghak.tvmazetest.models.Model;

import java.util.List;

public interface OnModelListReceivedListener<E extends Model> extends OnReceivedListener {
    void onSuccess(int requestId, int statusCode, List<E> receivedList);
}
