package com.otaghak.tvmazetest.api.listeners;

public interface OnReceivedListener {
    void onFailure(int requestId, int statusCode);

    boolean isAlive();
}
