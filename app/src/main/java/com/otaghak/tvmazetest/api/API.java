package com.otaghak.tvmazetest.api;

import android.content.Context;
import android.net.ConnectivityManager;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.builder.Builders;
import com.otaghak.tvmazetest.MainActivity;
import com.otaghak.tvmazetest.api.utils.StatusCode;
import com.otaghak.tvmazetest.models.Model;

import org.json.JSONObject;


public class API {
    private MainActivity mainActivity;

    public API(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    private boolean checkInternetConnection() {
        ConnectivityManager cm = (ConnectivityManager) mainActivity.getSystemService(Context.CONNECTIVITY_SERVICE);
        return cm == null || cm.getActiveNetworkInfo() != null;
    }

    public <E extends Model> void request(Request<E> request) {
        if (checkInternetConnection()) {
            Builders.Any.B b;
            String url = request.getAbsoluteUrl();
            switch (request.getMethod()) {
                case Request.METHOD_GET:

                    b = Ion.with(mainActivity).load("GET", url);
                    b.addQueries(request.getQuery())
                            .asByteArray()
                            .withResponse()
                            .setCallback(request);
                    break;
                case Request.METHOD_POST:
                    b = Ion.with(mainActivity).load("POST", url);

                    if (request.getParams() != null) {
                        RequestParams params = request.getParams();
                        JSONObject jsonObject = params.getJson();
                        JsonParser parser = new JsonParser();
                        JsonObject o = parser.parse(jsonObject.toString()).getAsJsonObject();
                        b.setJsonObjectBody(o);
                    }
                    b.asByteArray()
                            .withResponse()
                            .setCallback(request);
                    break;
            }
        } else {
            request.getListener().onFailure(request.getId(), StatusCode.NO_INTERNET_CONNECTION);
        }

    }


}

