package com.otaghak.tvmazetest.api;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class RequestParams {
    private JSONObject json;

    public RequestParams() {
        json = new JSONObject();
    }

    public void put(String key, String value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String key, int value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String key, boolean value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public void put(String key, double value) {
        try {
            json.put(key, value);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public JSONObject getJson() {
        return json;
    }

    public HashMap<String, List<String>> getQuery() {
        HashMap<String, List<String>> toReturn = new HashMap<>();
        for (Iterator<String> it = json.keys(); it.hasNext(); ) {
            String key = it.next();
            try {
                Object o = json.get(key);
                ArrayList<String> toPut = new ArrayList<>();
                toPut.add(o.toString());
                toReturn.put(key, toPut);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return toReturn;
    }
}
