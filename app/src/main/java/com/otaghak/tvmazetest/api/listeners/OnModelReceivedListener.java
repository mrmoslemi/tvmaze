package com.otaghak.tvmazetest.api.listeners;


import com.otaghak.tvmazetest.models.Model;

public interface OnModelReceivedListener<E extends Model> extends OnReceivedListener {
    void onSuccess(int requestId, int statusCode, E receivedModel);
}
