package com.otaghak.tvmazetest.views.searchFragment;

import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.widget.GridView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.api.Request;
import com.otaghak.tvmazetest.api.RequestParams;
import com.otaghak.tvmazetest.api.listeners.OnModelListReceivedListener;
import com.otaghak.tvmazetest.api.utils.StatusCode;
import com.otaghak.tvmazetest.api.utils.URLs;
import com.otaghak.tvmazetest.models.Message;
import com.otaghak.tvmazetest.models.ShowSearchResult;
import com.otaghak.tvmazetest.views.AbstractFragment;
import com.otaghak.tvmazetest.views.commonViews.LoadingViewHolder;

import java.util.List;

public class SearchFragment extends AbstractFragment implements SearchView.OnQueryTextListener, OnModelListReceivedListener<ShowSearchResult> {
    private Toolbar toolbar;
    private int lastQueryId;
    private String lastQuery;
    private ShowSearchResultListAdapter adapter;
    private LoadingViewHolder loading;
    private MessageModelViewHolder message;

    @Override
    public int getLayoutResId() {
        return R.layout.search_fragment_layout;
    }

    @Override
    public void initialArguments() {

    }

    @Override
    public void initialViews() {
        toolbar = (Toolbar) findViewById(R.id.searchFragmentToolbar);
        setupSearchView();
        setupGridView();
        loading = new LoadingViewHolder(findViewById(R.id.searchFragmentLoadingView));
        message = new MessageModelViewHolder(findViewById(R.id.searchFragmentEmptyView));
        setLoadingState(LoadingViewHolder.STATE_LOADED);
    }

    @Override
    public void onShowActions() {
        setupToolbar(toolbar);
    }

    private void setLoadingState(int state) {
        loading.setState(state);
        if (state == LoadingViewHolder.STATE_LOADED) {
            message.setVisible(adapter.isEmpty());
            if (adapter.isEmpty()) {
                if (lastQuery == null || lastQuery.isEmpty()) {
                    message.setModel(Message.getQueryIsEmptyMessage());
                } else {
                    message.setModel(Message.getResultIsEmptyMessage());
                }
            }
        }
    }

    private void setupSearchView() {
        SearchView searchView = (SearchView) findViewById(R.id.searchSearchView);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
    }

    private void setupGridView() {
        GridView gridView = (GridView) findViewById(R.id.searchFragmentGridView);
        adapter = new ShowSearchResultListAdapter(getContext());
        gridView.setAdapter(adapter);
    }

    private void requestNewQuery(String query) {
        Request<ShowSearchResult> searchQueryRequest = new Request<>(Request.METHOD_GET, URLs.SEARCH + URLs.SHOWS, ShowSearchResult.class, this);
        RequestParams searchQueryParams = new RequestParams();
        searchQueryParams.put("q", query);
        searchQueryRequest.setParams(searchQueryParams);
        lastQueryId = searchQueryRequest.getId();
        lastQuery = query;
        setLoadingState(LoadingViewHolder.STATE_LOADING);
        request(searchQueryRequest);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return true;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        requestNewQuery(newText);
        return true;
    }

    @Override
    public void onSuccess(int requestId, int statusCode, List<ShowSearchResult> receivedList) {
        if (requestId == lastQueryId) {
            adapter.clear();
            adapter.addAll(receivedList);
            adapter.notifyDataSetChanged();
            setLoadingState(LoadingViewHolder.STATE_LOADED);
        }
    }

    @Override
    public void onFailure(int requestId, int statusCode) {
        if (requestId == lastQueryId) {
            if (statusCode == StatusCode.NO_INTERNET_CONNECTION) {
                setLoadingState(LoadingViewHolder.STATE_NO_INTERNET);
            } else {
                setLoadingState(LoadingViewHolder.STATE_FAILED);
            }
        }
    }
}
