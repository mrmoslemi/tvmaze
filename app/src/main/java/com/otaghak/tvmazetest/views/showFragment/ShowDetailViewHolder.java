package com.otaghak.tvmazetest.views.showFragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.views.AbstractViewHolder;

public class ShowDetailViewHolder extends AbstractViewHolder {
    private TextView value;

    ShowDetailViewHolder(View rootView, int iconResId, int titleResId) {
        super(rootView);
        ImageView icon = (ImageView) findViewById(R.id.iconTitleValueIcon);
        icon.setImageResource(iconResId);
        TextView title = (TextView) findViewById(R.id.iconTitleValueTitle);
        title.setText(titleResId);
        value = (TextView) findViewById(R.id.iconTitleValueValue);
    }

    public void setValue(String value) {
        this.value.setText(value);
    }

}
