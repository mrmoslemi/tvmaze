package com.otaghak.tvmazetest.views.searchFragment;

import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.models.Show;
import com.otaghak.tvmazetest.views.AbstractModelViewHolder;
import com.otaghak.tvmazetest.views.commonViews.DualImageModelViewHolder;
import com.otaghak.tvmazetest.views.showFragment.ShowFragment;

public class ShowModelViewHolder extends AbstractModelViewHolder<Show> implements View.OnClickListener {
    private DualImageModelViewHolder image;
    private TextView name;
    private TextView rating;
    private TextView language;

    ShowModelViewHolder(ViewGroup parent, int layoutResId) {
        super(parent, layoutResId);
        image = new DualImageModelViewHolder(findViewById(R.id.showSearchResultImage), R.drawable.video_placeholder, 3, 0);
        name = (TextView) findViewById(R.id.showSearchResultName);
        rating = (TextView) findViewById(R.id.showSearchResultRate);
        language = (TextView) findViewById(R.id.showSearchResultLanguage);
        setOnClickListener(this);
    }

    @Override
    public void onModelChange() {
        image.setModel(getModel().image);
        name.setText(getModel().name);
        rating.setText(getRatingText());
        language.setText(getModel().language);
    }

    private String getRatingText() {
        if (getModel().rating != null) {
            return getModel().rating.toString();
        } else {
            return getStringValue(R.string.nothing);
        }
    }

    @Override
    public void onClick(View v) {
        ShowFragment showFragment = ShowFragment.createInstance(getModel().id);
        pushFragment(showFragment);
    }
}
