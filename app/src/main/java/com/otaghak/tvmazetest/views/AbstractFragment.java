package com.otaghak.tvmazetest.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.CallSuper;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.otaghak.tvmazetest.MainActivity;
import com.otaghak.tvmazetest.MainView;
import com.otaghak.tvmazetest.api.Request;

public abstract class AbstractFragment extends Fragment implements ViewController {
    protected Bundle savedInstanceState;
    private View rootView;
    private boolean alive = true;
    private LayoutInflater inflater;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.inflater = inflater;
        this.savedInstanceState = savedInstanceState;
        if (rootView == null) {
            initialArguments();
            rootView = inflater.inflate(getLayoutResId(), container, false);
            initialViews();
        }
        onShowActions();
        return rootView;
    }

    @Override
    public void onViewCreated(final View v, Bundle savedInstanceState) {

    }

    public Animation onCreateAnimation(int transit, boolean enter, int nextAnim) {
        Animation animation = super.onCreateAnimation(transit, enter, nextAnim);
        if (animation == null && nextAnim != 0) {
            animation = AnimationUtils.loadAnimation(getActivity(), nextAnim);
        }
        if (animation != null) {
            if (getView() != null) {
                getView().setLayerType(View.LAYER_TYPE_HARDWARE, null);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    public void onAnimationEnd(Animation animation) {
                        if (getView() != null) {
                            getView().setLayerType(View.LAYER_TYPE_NONE, null);
                        }
                        if (getMainView() != null) {
                            getMainView().resume();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
            }
        }

        return animation;
    }

    public void setupToolbar(Toolbar toolbar) {
        getMainActivity().setSupportActionBar(toolbar);
        getMainActivity().getSupportActionBar().setDisplayHomeAsUpEnabled(getMainView().hasBack());
        getMainActivity().getSupportActionBar().setDisplayShowHomeEnabled(getMainView().hasBack());
    }

    public abstract int getLayoutResId();

    public abstract void initialArguments();

    public abstract void initialViews();

    public void onShowActions() {

    }


    public Bundle getSavedInstanceState() {
        return savedInstanceState;
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public View findViewById(int viewResId) {
        if (getRootView() == null) {
            return null;
        } else {
            return getRootView().findViewById(viewResId);
        }
    }

    @Override
    public View inflate(int layoutResId, ViewGroup parent) {
        return inflater.inflate(layoutResId, parent, false);
    }

    @Override
    public Context getContext() {
        if (getRootView() == null) {
            return null;
        } else {
            return getRootView().getContext();
        }
    }

    @Override
    public MainActivity getMainActivity() {
        if (getContext() == null) {
            return null;
        } else {
            return (MainActivity) getContext();
        }
    }

    @Override
    public MainView getMainView() {
        if (getMainActivity() == null) {
            return null;
        } else {
            return getMainActivity().getMainView();
        }
    }

    @Override
    public Drawable getDrawable(int drawableResId) {
        if (getContext() == null) {
            return null;
        } else {
            return getContext().getResources().getDrawable(drawableResId);
        }
    }

    @Override
    public String getStringValue(int id) {
        if (getContext() == null) {
            return null;
        } else {
            return getContext().getResources().getString(id);
        }
    }

    @Override
    public void pushFragment(AbstractFragment fragment) {
        getMainView().pushFragment(fragment);
    }

    @Override
    public void popFragment() {
        getMainView().popFragment();
    }

    @Override
    public void request(Request request) {
        if (getMainActivity() != null) {
            getMainActivity().request(request);
        }
    }

    @Override
    @CallSuper
    public void kill() {
        rootView = null;
        inflater = null;
        alive = false;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

}
