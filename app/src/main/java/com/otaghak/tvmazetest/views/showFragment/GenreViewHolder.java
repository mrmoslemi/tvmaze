package com.otaghak.tvmazetest.views.showFragment;

import android.view.ViewGroup;
import android.widget.TextView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.views.AbstractViewHolder;

class GenreViewHolder extends AbstractViewHolder {
    GenreViewHolder(ViewGroup parent, String genreName) {
        super(parent, R.layout.genre_view_holder_layout);
        ((TextView)getRootView()).setText(genreName);
    }
}
