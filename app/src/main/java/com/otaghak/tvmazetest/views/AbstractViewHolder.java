package com.otaghak.tvmazetest.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.annotation.CallSuper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.otaghak.tvmazetest.MainActivity;
import com.otaghak.tvmazetest.MainView;
import com.otaghak.tvmazetest.api.Request;

public abstract class AbstractViewHolder implements ViewController {
    private View rootView;
    private LayoutInflater inflater;
    private Context context;
    private boolean alive = true;


    public AbstractViewHolder(View rootView) {
        this.rootView = rootView;
        this.context = rootView.getContext();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public AbstractViewHolder(ViewGroup parent, int layoutID) {
        this.context = parent.getContext();
        this.inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.rootView = inflate(layoutID, parent);
    }

    public void setOnClickListener(View.OnClickListener listener) {
        rootView.setFocusable(true);
        rootView.setClickable(true);
        rootView.setOnClickListener(listener);
    }

    public void setVisible(boolean visible) {
        if (visible) {
            rootView.setVisibility(View.VISIBLE);
        } else {
            rootView.setVisibility(View.GONE);
        }
    }

    @Override
    public View getRootView() {
        return this.rootView;
    }

    @Override
    public View findViewById(int viewResId) {
        if (getRootView() == null) {
            return null;
        } else {
            return getRootView().findViewById(viewResId);
        }
    }

    @Override
    public View inflate(int layoutResId, ViewGroup parent) {
        return inflater.inflate(layoutResId, parent, false);
    }

    @Override
    public Context getContext() {
        if (getRootView() == null) {
            return null;
        } else {
            return getRootView().getContext();
        }
    }

    @Override
    public MainActivity getMainActivity() {
        if (getContext() == null) {
            return null;
        } else {
            return (MainActivity) getContext();
        }
    }

    @Override
    public MainView getMainView() {
        if (getMainActivity() == null) {
            return null;
        } else {
            return getMainActivity().getMainView();
        }
    }

    @Override
    public Drawable getDrawable(int drawableResId) {
        if (getContext() == null) {
            return null;
        } else {
            return getContext().getResources().getDrawable(drawableResId);
        }
    }

    @Override
    public String getStringValue(int id) {
        if (getContext() == null) {
            return null;
        } else {
            return getContext().getResources().getString(id);
        }
    }

    @Override
    public void pushFragment(AbstractFragment fragment) {
        getMainView().pushFragment(fragment);
    }

    @Override
    public void popFragment() {
        getMainView().popFragment();
    }

    @Override
    public void request(Request request) {
        if (getMainActivity() != null) {
            getMainActivity().request(request);
        }
    }

    @Override
    @CallSuper
    public void kill() {
        alive = false;
        rootView = null;
        inflater = null;
        context = null;
    }

    @Override
    public boolean isAlive() {
        return alive;
    }

}
