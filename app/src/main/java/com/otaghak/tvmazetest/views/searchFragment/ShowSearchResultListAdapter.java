package com.otaghak.tvmazetest.views.searchFragment;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.models.ShowSearchResult;

public class ShowSearchResultListAdapter extends ArrayAdapter<ShowSearchResult> {
    ShowSearchResultListAdapter(@NonNull Context context) {
        super(context, 0);
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        ShowModelViewHolder viewHolder;
        if (convertView == null) {
            viewHolder = new ShowModelViewHolder(parent, R.layout.show_search_result_view_holder_layout);
            convertView = viewHolder.getRootView();
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ShowModelViewHolder) convertView.getTag();
        }
        viewHolder.setModel(getItem(position).show);
        return convertView;
    }
}
