package com.otaghak.tvmazetest.views.commonViews;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.views.AbstractViewHolder;

public class LoadingViewHolder extends AbstractViewHolder implements View.OnClickListener {
    public static final int STATE_LOADED = 0;
    public static final int STATE_NO_INTERNET = 1;
    public static final int STATE_FAILED = 2;
    public static final int STATE_LOADING = 3;

    private View loadingView;
    private View failedView;
    private TextView failureExplanation;
    private View.OnClickListener onFailedButtonClickListener;

    public LoadingViewHolder(View rootView) {
        super(rootView);
        loadingView = findViewById(R.id.loadingView_loading);
        failedView = findViewById(R.id.loadingView_failed);
        failureExplanation = (TextView) findViewById(R.id.loadingView_failed_subTitle);
        Button failedButton = (Button) findViewById(R.id.loadingView_failed_button);
        failedButton.setOnClickListener(this);
    }

    public void setOnFailedButtonClickListener(View.OnClickListener listener) {
        this.onFailedButtonClickListener = listener;
    }

    public void setState(int state) {
        if (state == STATE_NO_INTERNET) {
            failureExplanation.setText(R.string.checkYourInternetConnection);
        } else if (state == STATE_FAILED) {
            failureExplanation.setText(getStringValue(R.string.tryAgainMessage));
        }
        switch (state) {
            case STATE_LOADED:
                setVisible(false);
                break;
            case STATE_LOADING:
                setVisible(true);
                loadingView.setVisibility(View.VISIBLE);
                failedView.setVisibility(View.GONE);
                break;
            case STATE_NO_INTERNET:
            case STATE_FAILED:
                setVisible(true);
                loadingView.setVisibility(View.GONE);
                failedView.setVisibility(View.VISIBLE);
                break;
        }
    }

    @Override
    public void onClick(View v) {
        if (onFailedButtonClickListener != null) {
            onFailedButtonClickListener.onClick(this.getRootView());
        }
    }
}
