package com.otaghak.tvmazetest.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.view.ViewGroup;

import com.otaghak.tvmazetest.MainActivity;
import com.otaghak.tvmazetest.MainView;
import com.otaghak.tvmazetest.api.Request;


public interface ViewController {
    View getRootView();

    View findViewById(int viewResId);

    View inflate(int layoutResId, ViewGroup parent);

    Context getContext();

    MainActivity getMainActivity();

    MainView getMainView();

    Drawable getDrawable(int drawableResId);

    String getStringValue(int stringResId);

    void pushFragment(AbstractFragment fragment);

    void popFragment();

    void request(Request request);

    void kill();

    boolean isAlive();
}
