package com.otaghak.tvmazetest.views.commonViews;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.widget.ImageView;

import com.otaghak.tvmazetest.Utils;
import com.otaghak.tvmazetest.models.DualImage;
import com.otaghak.tvmazetest.views.AbstractModelViewHolder;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class DualImageModelViewHolder extends AbstractModelViewHolder<DualImage> {
    private int placeHolderResId;
    private int mediumTryCount;
    private int originalTryCount;
    private int remainingMediumTryCount;
    private int remainingOriginalTryCount;

    public DualImageModelViewHolder(View rootView, int placeHolderResId, int mediumTryCount, int originalTryCount) {
        super(rootView);
        this.placeHolderResId = placeHolderResId;
        this.mediumTryCount = mediumTryCount;
        this.originalTryCount = originalTryCount;
    }

    @Override
    public void onModelChange() {
        reset();
        tryLoadingMedium();
    }

    private void reset() {
        ((ImageView) getRootView()).setImageResource(placeHolderResId);
        remainingMediumTryCount = mediumTryCount;
        remainingOriginalTryCount = originalTryCount;
    }

    private void tryLoadingMedium() {
        if (getModel() != null && !Utils.checkNull(getModel().medium)) {
            if (remainingMediumTryCount > 0) {
                remainingMediumTryCount--;
                Picasso.with(getContext()).load(getModel().medium).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (isAlive()) {
                            ((ImageView) getRootView()).setImageBitmap(bitmap);
                            tryLoadingOriginal();
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        if (isAlive()) {
                            tryLoadingMedium();
                        }
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }
        }
    }

    private void tryLoadingOriginal() {
        if (getModel() != null && !Utils.checkNull(getModel().original)) {
            if (remainingOriginalTryCount > 0) {
                remainingOriginalTryCount--;
                Picasso.with(getContext()).load(getModel().original).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (isAlive()) {
                            ((ImageView) getRootView()).setImageBitmap(bitmap);
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {
                        if (isAlive()) {
                            tryLoadingOriginal();
                        }
                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }
        }
    }
}
