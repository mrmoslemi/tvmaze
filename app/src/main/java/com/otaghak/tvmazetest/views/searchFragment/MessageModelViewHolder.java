package com.otaghak.tvmazetest.views.searchFragment;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.models.Message;
import com.otaghak.tvmazetest.views.AbstractModelViewHolder;

public class MessageModelViewHolder extends AbstractModelViewHolder<Message> {
    private ImageView image;
    private TextView title;
    private TextView subTitle;

    MessageModelViewHolder(View view) {
        super(view);
        image = (ImageView) findViewById(R.id.attentionViewImage);
        title = (TextView) findViewById(R.id.attentionViewTitle);
        subTitle = (TextView) findViewById(R.id.attentionViewSubtitle);
    }

    @Override
    public void onModelChange() {
        image.setImageResource(getModel().imageResId);
        title.setText(getModel().titleResId);
        subTitle.setText(getModel().subTitleResId);
    }
}
