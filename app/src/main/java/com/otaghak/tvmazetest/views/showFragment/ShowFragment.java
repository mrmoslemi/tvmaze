package com.otaghak.tvmazetest.views.showFragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.otaghak.tvmazetest.R;
import com.otaghak.tvmazetest.Utils;
import com.otaghak.tvmazetest.api.RequestParams;
import com.otaghak.tvmazetest.api.utils.URLs;
import com.otaghak.tvmazetest.models.Show;
import com.otaghak.tvmazetest.views.AbstractModelListenerFragment;
import com.otaghak.tvmazetest.views.commonViews.DualImageModelViewHolder;

import java.util.ArrayList;

public class ShowFragment extends AbstractModelListenerFragment<Show> implements View.OnClickListener {
    private static final String SHOW_ID = "SHOW_ID";
    private int showId;

    private Toolbar toolbar;
    private DualImageModelViewHolder image;
    private RatingBar rating;

    private ShowDetailViewHolder schedule;
    private ShowDetailViewHolder status;
    private ShowDetailViewHolder network;
    private ShowDetailViewHolder type;
    private ShowDetailViewHolder premiered;
    private ShowDetailViewHolder language;

    private TextView summary;

    private ArrayList<GenreViewHolder> genres;
    private LinearLayout genresContainer;

    private View websiteContainer;

    public static ShowFragment createInstance(int showId) {
        ShowFragment showFragment = new ShowFragment();
        Bundle args = new Bundle();
        args.putInt(SHOW_ID, showId);
        showFragment.setArguments(args);
        return showFragment;
    }

    @Override
    public int getContentLayoutResId() {
        return R.layout.show_fragment_content_layout;
    }

    @Override
    public void initialContentViews() {
        toolbar = (Toolbar) findViewById(R.id.showTitle);
        image = new DualImageModelViewHolder(findViewById(R.id.showImage), R.drawable.video_placeholder, 3, 3);

        rating = (RatingBar) findViewById(R.id.showRating);

        schedule = new ShowDetailViewHolder(findViewById(R.id.showSchedule), R.drawable.ic_time, R.string.schedule);
        status = new ShowDetailViewHolder(findViewById(R.id.showStatus), R.drawable.ic_state, R.string.status);
        network = new ShowDetailViewHolder(findViewById(R.id.showNetwork), R.drawable.ic_network, R.string.network);
        type = new ShowDetailViewHolder(findViewById(R.id.showType), R.drawable.ic_type, R.string.type);
        premiered = new ShowDetailViewHolder(findViewById(R.id.showPremier), R.drawable.ic_date, R.string.premiered);
        language = new ShowDetailViewHolder(findViewById(R.id.showLanguage), R.drawable.ic_language, R.string.language);

        summary = (TextView) findViewById(R.id.showSummary);
        genres = new ArrayList<>();
        genresContainer = (LinearLayout) findViewById(R.id.showGenres);

        websiteContainer = findViewById(R.id.showWebsiteContainer);
        Button website = (Button) findViewById(R.id.showWebsite);
        website.setOnClickListener(this);
    }


    @Override
    public void onAppear() {
        setupToolbar(toolbar);
    }

    @Override
    public void onLoaded() {
        toolbar.setTitle(getModel().name);
        image.setModel(getModel().image);

        rating.setRating(getRatingValue());

        schedule.setValue(getScheduleValue());
        status.setValue(getModel().status);
        network.setValue(getNetworkValue());
        type.setValue(getModel().type);
        premiered.setValue(getModel().premiered);
        language.setValue(getModel().language);

        setSummaryText();
        setGenres();

        setWebsiteButton();
    }

    private float getRatingValue() {
        if (getModel().rating != null) {
            return getModel().rating.getAverageRating();
        } else {
            return 0;
        }
    }

    private String getScheduleValue() {
        if (getModel().schedule != null) {
            return getModel().schedule.toString();
        } else {
            return getStringValue(R.string.nothing);
        }
    }

    private String getNetworkValue() {
        if (getModel().network != null) {
            return getModel().network.toString();
        } else {
            return getStringValue(R.string.nothing);
        }
    }

    private void setSummaryText() {
        if(!Utils.checkNull(getModel().summary)){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            summary.setText(Html.fromHtml(getModel().summary, Html.FROM_HTML_MODE_COMPACT));
        } else {
            summary.setText(Html.fromHtml(getModel().summary));
        }}else {
            summary.setText(getStringValue(R.string.nothing));
        }
    }

    private void setGenres() {
        for (GenreViewHolder genreViewHolder : genres) {
            genreViewHolder.kill();
        }
        genresContainer.removeAllViews();
        for (String genreName : getModel().genres) {
            GenreViewHolder genreViewHolder = new GenreViewHolder(genresContainer, genreName);
            genresContainer.addView(genreViewHolder.getRootView());
            genres.add(genreViewHolder);
        }
    }

    private void setWebsiteButton() {
        if (Utils.checkNull(getModel().officialSite)) {
            websiteContainer.setVisibility(View.GONE);
        } else {
            websiteContainer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onClick(View v) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(getModel().officialSite));
        startActivity(i);
    }

    @Override
    public String getRelativeUrl() {
        return URLs.SHOWS + showId;
    }

    @Override
    public RequestParams getRequestParams() {
        return null;
    }

    @Override
    public Class<Show> getModelClass() {
        return Show.class;
    }

    @Override
    public void initialArguments() {
        assert getArguments() != null;
        showId = getArguments().getInt(SHOW_ID);
    }

}
